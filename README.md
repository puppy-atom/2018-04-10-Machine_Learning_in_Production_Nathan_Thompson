# 2018-04-10 Machine Learning in Production
### Nathan Thompson

ATOM is meeting on Tuesday, 10th April, 6:30pm, at Galvanize!

Join us for a highly participative discussion !

This week, we'll cover some of the challenges of running machine learning in a production setting. These papers from Google offer a nicely-detailed perspective on the topic, one we'll enrich with the group’s own horror and success stories. Nathan Thompson will be leading our discussion.

The ML Test Score:
https://static.googleusercontent.com/media/research.google.com/en//pubs/archive/46555.pdf

Hidden Technical Debt in Machine Learning Systems
https://papers.nips.cc/paper/5656-hidden-technical-debt-in-machine-learning-systems.pdf

About ATOM:
Advanced Topics on Machine learning ( ATOM ) is a learning and discussion group for cutting-edge machine learning techniques in the real world. We work through winning Kaggle competition entries or real-world ML projects, learning from those who have successfully applied sophisticated data science pipelines to complex problems.

As a discussion group, we strongly encourage participation, so be sure to read up about the topic of conversation beforehand !

ATOM can be found on PuPPy’s Slack under the channel #atom, and on PuPPy’s Meetup.com events.

We're kindly hosted by Galvanize (https://www.galvanize.com). Thank you !